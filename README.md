# get-lufs-from-wav

## Description

Module providing a function that calculates the loudness of wav file.

## Pip Packages

- soundfile
- pyloudnorm

## Terminal Syntax

```
python get_lufs.py <WAV FILE>
```
