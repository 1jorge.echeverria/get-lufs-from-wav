"""
Module providing a function that calculates the loudness of wav file.

Author: @1jorge.echeverria
sys.argv[1]: WAV path.
"""

import sys
import soundfile as sf
import pyloudnorm as pyln


def __get_wav_file_loudness():
    """
    Calculates the loudness of wav file using a BS.1770 meter.

    Returns:
        numpy.float64:
    """
    data, rate = sf.read(
        sys.argv[1])  # load audio (with shape (samples, channels))
    return (pyln.Meter(rate)).integrated_loudness(data)


if __name__ == "__main__":
    print(f"Loudness: {__get_wav_file_loudness()}")
